# Threat Intelligence @ City

Unless otherwise noted, everything in this repository is TLP:RED 

![tlp:red](https://www.cisa.gov/sites/default/files/tlp/TLP-icon-red.png)

Recipients may not share TLP:RED information with any parties outside of the specific exchange, meeting, or conversation in which it was originally disclosed.

https://www.cisa.gov/tlp

### What is this repository for? ###

Threat Intel is the process by which information is obtained, analysed, oriented on, and acted on by the CCS team.

This repo will contain threat information artifacts.


### Contribution guidelines ###

Watch this space

### Who do I talk to? ###

* Repo owner or admin
  
---
marp: false

---

# __City of Gold Coast__
#### Situation Report [HOT BEAR]. Escalating geopolitical tensions, and resulting cyber impacts for CoGC


|     Unique   Identifier:    	|     [SITREP_2022_HOT_BEAR]             	|
|-----------------------------	|----------------------------------------	|
|     Report Date:            	|     08/03/2022                         	|
|     Time:                   	|     12PM                               	|
|     Report   Author:        	|     Rhys Weightman                       	|
|     Email:                  	|     rweightman@goldcoast.qld.gov.au    	|
|     Version:                	|     V0.2                               	|
|     Status:                 	|     Draft                              	|
|     TLP:                    	|     Red                                 	|

---

### Situation Summary

CCS assess that the threat is unlikely to impact City at this point in time. It is forecasted that there is a chance that threat activity could increase against City over the next months as the war between Ukrain and Russia evolves.

Russia has approved a list of foreign states commit unfriendly actions against Russia, including: Australia, UK, EU countries, Iceland, Canada, Liechtenstein, Monaco, New Zealand, Norway, SKorea, San Marino, Singapore, USA, Taiwan, Ukraine, Montenegro, Switzerland, Japan. [16]

On 23rd of February, the City of Gold Coast’s Corporate Cyber Security (CCS) team has been provided advice on malicious cyber incidents that have been occurring in the world as a result of the current threat environment. Specifically, this advice has been provided in response to the escalating geopolitical tensions between Russia and Ukraine.

CCS have previously noted two common themes defining our threat landscape as part of our 2020 External Threat Landscape Report:

- An evolution of cybercrime as a business, focusing on cheap and quick to launch phishing and malware campaigns, with a goal of financial gains through black-market trading by criminal groups.
- A darker second theme of state-sponsored attacks against City through state-funded Advanced Persistent Threats (APTs), focused on utilising off-the-shelf exploits, with a goal of gaining and maintaining persistent access to City services. 

<div style="page-break-after: always"></div>

---

### Update: 08/03/2022

The conflict has provided an unveling of Russian state-sponsored cyber criminals. Anonymous researchers or disgruntled cybercrime gang members have leaked instant messaging chat logs (Jabber), hacking tools, IT infrastructure, Bitcoin addresses, screenshots, training material and source code for the largest cybercriminal group in the world. 

This group is named as the Conti Cybercriminal group, a.k.a "Wizard Spider". [11]

The leaks reveal the following:

-    Over 70 affliliates + core Conti group members enabled Ransomware as a Service (RaaS) along with Banking Trojans.
-    From April 21st, 2017 - February 28th, 2022 Conti has processed 65,498.197 BTC. That is 2,707,466,220.29 USD
     -    The core Conti group has made approximately 180,000,000 USD per year [12]
-    Additional Cybercriminal groups have now been linked as Conti: Ryuk and TrickBot
-    12 real person identities leaked along with handles and alias's for group members. [13]    
-    The threat actor group "Wizard Spider" is one of 5 groups that actively target Australian organisations. [14]
-    This group was highly organised with good operational security techniques and practices. Each group member was a salaried person.

On January 15, 2022, the Microsoft Threat Intelligence Center (MSTIC) disclosed that destructive data-wiping malware disguised as ransomware known as WhisperGate was being used to target organizations in Ukraine [9]. This can now be qualitatively attributed to the Conti group.


---

### Initial Reporting: 07/03/2022

Our Managed Security Service Provider, CCX, the Australian Cyber Security Center (ACSC), and Cyber and Infrastructure Security Centre (CISC) have provided additional specific information:

- Cybercriminals associated with the Conti Ransomware have stated that they will indiscriminately target critical infrastructure in response to cyber or military action against Russia. [1]
- CISC recommends implementation of the ACSC Essential Eight level to a one or higher, or a similar cyber-security standard, and to review the Indicators of Compromise (IoC) provided by the ACSC. [2]
- Our MSSP, CCX, have identified that collateral damage from state-sponsored attacks could spil outside Ukraine and imapct City as was similarly the case with the NotPetya ransomware in late 2017/2018

The Australian Federal Government states that it will be supporting Ukraine [4]. This in turn raises the likelihood that Australian organisations will be targeted by Russian state actors or state-backed Cybercriminal groups. Some media organisations have experienced outages as a result of reporting on the conflict.

It is expected that City will see increase in both volume and types of threat activity as a result of this intelligence.

<div style="page-break-after: always"></div>

---

### 1.   Actions Taken

Actions taken for the period of the SITREP:

-   Moved to a heightened awareness cyber security posture, actively engaging threat information sharing partners.
-   Reviewed ACSC v7 of Advisory 2022-02. [3]
-   Our MSSP, CCX, has implemented additional security event monitoring of Russian-based threat actors [5]
-	CCS has implemented blocks of Discord, a known IOC for Russian C2 servers [6]
-	Proactively reached out to key City internal customers providing position of heightened awareness of cyber conflict.
-   Blocked "Discord" content distribution website - known malware delivery. [3][5]
-   CCS analysed and collated Conti leak data:
    -    Bitcoin addresses used by the Conti ransomware gang 
    -    Email address accounts known to have been used by members of the Conti ransomware gang.
    -    a complete list of Conti ransomware gang Dark Web onion XMPP users:
    -    a list of all the IPs found in the leaked internal communicatioin of the Conti ransomware gang.
    -    a complete list of all the URLs found in the internal leaked communicatioin of the Conti ransomware gang obtained using public sources.
    -    a full list of personal address accounts which are known to have been used by members of the Conti ransomware gang.
    -    the full list of all the Conti ransomware gang affiliates and Dark Web onion XMPP server users.
    -    the full list of all the members of the Conti ransomware gang obtained using public sources.
    -    the currently active XMPP Dark Web onion server users of the Conti ransomware gang:
    -    the publicly accessible URLs obtained using public sources from the leaked internal communication of the Conti ransomware gang.
    -    Conti ransomware gang's primary Dark Web Onion XMPP:
    -    Sample IP addresses obtained from internal Conti ransomware gang communications include:
    -    Sample email addresses from leaks
    -    Related responding domains known to have participated in Conti ransomware gang's C&C (Command and Control) and Internet connected infrastructure include:
    -    Related malicious URls known to have participated in Conti ransomware gang's C&C (Command and Control) and Internet connected infrastructure include:

<div style="page-break-after: always"></div>

### 2.	Actions Underway

-   Akamai made a new WAF feature available called “Geo Controls for Uncertain Conditions”. This can be used to Alert/Deny breakaway traffic. CCS investigating use. [8]
-   CCS scanning for detection of ACSC/CISA vulnerabilities [10]
    CCS is further investigating the evolving IOCs provided by the ACSC(v7) and CISA advisories [3] [5].
-	Continue to engage threat information sharing partners.

### 3.	Actions Planned 
-   Provide an updated SITREP on 14th March or sooner as events unfold.
-   MSSP, CCX, to perform threat hunting for Russian-based threats over the coming days and weeks.
-   Primary use of Firefox Send, file io, and qaz domains for phishing. Investigating impact of blocking these services. [15]

### 4.	Issues
-	Malware C2 server DNS queries are not currently blockable. Expect that City would have to engage and configure a Protective DNS service suchas as that provided by CITEC, Queensland Government. [7]


---
### Appendix B: References


[1] [ACSC: Conti Ransomware Activity and Advice](https://www.cyber.gov.au/acsc/view-all-content/advisories/ransomware-profile-conti)

[2] [CISC: Address to Australian Critical Infrastructure Owners](https://www.cisc.gov.au/help-and-support-subsite/Files/australian-critical-infrastructure-owners-operators-address.pdf) 

[3] [ACSC: Enhanced Cyber Security Posture](https://www.cyber.gov.au/acsc/view-all-content/advisories/2022-02-australian-organisations-should-urgently-adopt-enhanced-cyber-security-posture)

[4] [ABC: Australia promises cyber support to Ukraine](https://www.abc.net.au/news/2022-02-21/ukraine-australia-cyberattack-russia-war-cybersecurity/100846870)

[5] [CISA: IOCs](https://www.cisa.gov/uscert/ncas/alerts/aa22-011a)

[5] [CISA: IOCs](https://www.cisa.gov/uscert/ncas/alerts/aa22-057a)

[6] [Unit 42: WhisperGate Malware](https://unit42.paloaltonetworks.com/ukraine-cyber-conflict-cve-2021-32648-whispergate/)

[7] [Queensland Government Protective DNS](https://www.qgcio.qld.gov.au/information-on/cyber-security/cyber-security-services/queensland-government-departments/protective-dns-service-description)

[8] [Akamai WAF](https://control.akamai.com/apps/auth/?SAMLRequest=fZHLTsMwEEXX%2FEXkfZ6Gph0lkYq6oBKIiEYs2KBJ4lKL2A4eR%2BrnkwdI7abejX1mru%2BdjFB1PWwHd9Jv4mcQ5Lyz6jTB%2FJCzwWowSJJAoxIEroHD9uUZkiCC3hpnGtMxbzx3u7FXanTS6JydnOsJwrAx2lnTBfiNCmXQGBVi31OIo2A49%2B13OfsUWD%2BseMubDW%2FjNMJVejzGaxR1u66RJ5uFJBrEXpND7XKWREniR9yP0irikKRwH3%2FMWPn3q0epW6m%2FbluoF4jgqapKv3w9VMx7F5ZmEyPAimwKAmZtexHN7bFIJOyUBCsmbHI7Xs4Z%2BFZgq8R%2FJc5OWI1dFl7oFEt1vZfiFw%3D%3D&RelayState=7WLOKuONzRFnL59scmEX-XcImZwv4txE&SigAlg=http%3A%2F%2Fwww.w3.org%2F2001%2F04%2Fxmldsig-more%23rsa-sha256&Signature=I%2BoWIcLbfzmm5TgmjlTX%2FtkW5xbEqqpN70gZusBDM20jMVeZYcih%2BiC9PmFtQqpk9Uead%2FNw9sbOClLTY9J3lvotU2No4IdnQk9ft19gOwe3nlhvFPcVrBazmrX%2FUZ8YxtatFNAKQJtfnuqaTBoZjQdmQNuzVPpE5kXbwQfAKYaE6iE%2BvvQ7COEFAk7n7rdbCceNNw64jLHthG%2Bq3j97oPWccq%2FHaCPl%2FLUCJU6WEfyV8ihwj%2FzLDhIXKiGUklu6eAqar0ID%2FpqOP6u3D62PbBYtvqRhQXs1RcLPDUiTxITUpH7XrMNDQm5IGFi4%2FtubQp5XsX05n4z7mcYtoFFBGw%3D%3D#/login)

[9] [MSIT WhisperGate](https://www.microsoft.com/security/blog/2022/01/15/destructive-malware-targeting-ukrainian-organizations/)

[10] [Tenable Security Advisory](https://www.tenable.com/blog/government-advisories-warn-of-apt-activity-resulting-from-russian-invasion-of-ukraine)

[11] [Techtarget Conti](https://www.techtarget.com/searchsecurity/news/252514047/Conti-ransomware-source-code-documentation-leaked)

[12] [Chainanlysis Ransomware](https://go.chainalysis.com/rs/503-FAP-074/images/Crypto-Crime-Report-2022.pdf)

[13] [Trickbot Leak Tracking OSINT: Github](https://github.com/Cybernite-Technologies/trickbot-leak)

[14] [Crowdstrike Threat Actor Profile: Wizard Spider](https://adversary.crowdstrike.com/en-US/adversary/wizard-spider/)

[15] [Twitter: Conti Analysis](https://twitter.com/S0fianeHamlaoui/status/1500843486978969612/photo/1)

[16] [Russia defines approved unfriendly countries](https://www.canberratimes.com.au/story/7648722/russian-govt-lists-hostile-countries/)